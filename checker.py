import re
import json

from rich.markdown import Markdown 

from datetime import datetime, timedelta

class Checker():  

    def __init__(self, bw_items, minimum_password_lenght=12, warn_period=182, critic_period=365, check_date=False, *args, **kwargs):
        super(Checker, self).__init__(*args, **kwargs)
        
        self.bw_items = bw_items
        self.warn_period = warn_period
        self.critic_period = critic_period
        self.minimum_password_lenght = minimum_password_lenght
        
        self.bad_passwords = []
        self.redundant_passwords = []
        
        self.check_password_complexity()
        if check_date:
            self.check_password_last_revision()
        self.check_password_redundancy()
        
    def get_items_count(self):
        return len(self.bw_items)
    
    def get_report_bad_passwords(self, format="json"):
        if format == "json":
            return json.dumps(self.bad_passwords)
        elif format == "cli":
            result_str = "## Password complexity\n\n"
            for bw_item in self.bad_passwords:
                result_str += f"* {bw_item.get('name')}\n"
                for issue in bw_item.get('issues'):
                    for cle,value in issue.items():
                        result_str += f"\t* {cle}\n"
                        for description in value:
                            result_str += f"\t\t* {description}\n"
                            
            result_str += f"\nThere are {len(self.bad_passwords)} weak passwords"
            result_str += f"\n\n**{100 - int(len(self.bad_passwords)*100/self.get_items_count())} % of password are strong**"
            
            return Markdown(result_str)
        
    def get_report_redundant_passwords(self, format="json"):
        if format == "json":
            return json.dumps(self.redundant_passwords)
        elif format == "cli":
            result_str = "## Password redundancy"
            for redundant_password in self.redundant_passwords:
                result_str += f"\n\n**{len(redundant_password)}** identical passwords for:\n"
                for password in redundant_password:
                    result_str += f"* {password}\n"
                    
            result_str += f"\nThere are {len(self.redundant_passwords)} passwords used many times"
            result_str += f"\n\n**{100 - int(len(self.redundant_passwords)*100/self.get_items_count())} % of password are unique**"
            return Markdown(result_str)
    
    def check_password_redundancy(self):
        passwords = [
            item['login']['password']
            for item in self.bw_items
            if item.get('login') and item.get('login').get('password')
        ]
        passwords_count = dict([(n, passwords.count(n)) for n in set(passwords)])
        for cle,value in passwords_count.items():
            if value > 1:
                names = [
                    f"{bw_item['name']}"
                    for bw_item in self.bw_items
                    if bw_item.get('login')
                    and bw_item.get('login').get('password')
                    and bw_item['login']['password'] == cle
                ]

                self.redundant_passwords.append(names)
    
    def check_password_last_revision(self):
        for bw_item in self.bw_items:
            if bw_item.get('login'):
                if not bw_item.get('login').get('passwordRevisionDate'):
                    self.__add_issue(bw_item.get('name'), 'revision date', issues=["Unknow revision date"])
                else:
                    passwordRevisionDate = datetime.strptime(bw_item.get('login').get('passwordRevisionDate')[:-5], '%Y-%m-%dT%H:%M:%S')
                    if passwordRevisionDate < (datetime.now() - timedelta(days=self.critic_period)):
                        self.__add_issue(bw_item.get('name'), 'revision date', issues=[f"Password too old ( > {self.critic_period} days)",])
                    elif passwordRevisionDate < (datetime.now() - timedelta(days=self.warn_period)):
                        self.__add_issue(bw_item.get('name'), 'revision date', issues=[f"Password too old ( > {self.critic_period} days)",])
    
    def check_password_complexity(self):
        for bw_item in self.bw_items:
            if bw_item.get('login') and bw_item.get('login').get('password'):
                password_to_check = bw_item.get('login').get('password')
            check_result = self.__password_complexity_check(password_to_check, self.minimum_password_lenght)
            if not check_result.get("password_ok", False):
                
                issues = []

                if check_result.get("length_error"):
                    issues.append(f"Password too short.")
                if check_result.get("digit_error"):
                    issues.append(f"There is no digits.")
                if check_result.get("uppercase_error"):
                    issues.append(f"There is no uppercase.")
                if check_result.get("lowercase_error"):
                    issues.append(f"There is no lowercase.")
                if check_result.get("symbol_error"):
                    issues.append(f"There is no symbol.")

                self.__add_issue(bw_item.get('name'), 'complexity', issues)
                    
    def __add_issue(self, name, kind_of_issue, issues):
        """
        Adds an issue for an existing item. If the item does not exist it is created. 

        Args:
            name (str): name of the item
            kind_of_issue (str): kind of issue like 'complexity' or 'redundancy'
            issues (array): list of issues
        """
        is_in_bad_password = False
        for password in self.bad_passwords:
            if password.get('name') == name:
                is_in_bad_password = True
                if not password.get('issues'):
                    password['issues'] = []
                password['issues'].append({kind_of_issue: issues})  
        
        if not is_in_bad_password:
            self.bad_passwords.append({'name': name,
                                       'issues': [{kind_of_issue: issues}]})
        

    def __password_complexity_check(self, password, minimum_password_lenght):
        """
        Verify the strength of 'password'
        Returns a dict indicating the wrong criteria
        A password is considered strong if:
            8 characters length or more
            1 digit or more
            1 symbol or more
            1 uppercase letter or more
            1 lowercase letter or more
        """

        length_error = len(password) < minimum_password_lenght
        digit_error = re.search(r"\d", password) is None
        uppercase_error = re.search(r"[A-Z]", password) is None
        lowercase_error = re.search(r"[a-z]", password) is None
        symbol_error = re.search(r"\W", password) is None

        # overall result
        password_ok = not (
            length_error or digit_error or uppercase_error or lowercase_error or symbol_error)

        return {
            'password_ok': password_ok,
            'length_error': length_error,
            'digit_error': digit_error,
            'uppercase_error': uppercase_error,
            'lowercase_error': lowercase_error,
            'symbol_error': symbol_error,
        }
