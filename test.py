import json
import sys
import subprocess
import argparse
from rich.console import Console

from checker import Checker

def main(argv):
    parser = argparse.ArgumentParser(description='Checks your passwords stored in bitwarden vault')
    parser.add_argument('-t', '--token', dest='token', required=True, action='store', default=None,
                        help="Token (obtained with 'bw login' command")

    args = parser.parse_args()
    token = args.token

    result = subprocess.run(["bw", "list", "items", "--session", token], capture_output=True)
    items = json.loads(result.stdout)
    
    if not items[0].get('login'):
        exit(1)

    checker = Checker(items)
    console = Console()
    console.print(checker.get_report_redundant_passwords(format="cli"))
    console.print(checker.get_report_bad_passwords(format="cli"))

if __name__ == '__main__':
    main(sys.argv[1:])
