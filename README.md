# bitwarden-password-checker

## Description

Allows you to check the complexity of your passwords stored in your bitwarden vault.
Also displays the services for which you use the same password and can also check when your passwords were last updated.

Returns the list of security issues in json format or directly in the cli. 

The script has intentionally been kept short so that it is easy to audit - you should read it before running it!

## Dependencies

Requires the BitWarden CLI [bw](https://help.bitwarden.com/article/cli/#download--install).
Required to retrieve the bitwarden session token and the list of passwords.  

Uses the 3rd party [rich](https://github.com/willmcgugan/rich) library for improve the readability of the report.

## Usage

To get your token use [bw](https://help.bitwarden.com/article/cli/#download--install):

```bash
bw login --raw
```

Once you have the token you can export the list of your bitwarden items and initialise the Checker class with it. 

```bash
bw list items --session your_token
```

#### Example:

```python
from rich.console import Console

from checker import Checker

result = subprocess.run(["bw", "list", "items", "--session", token], capture_output=True)
items = json.loads(result.stdout)

checker = Checker(items)
console = Console()
console.print(checker.get_report_redundant_passwords(format="cli"))
console.print(checker.get_report_bad_passwords(format="cli"))
```

You can use `python test.py -t 'session_token'` to initialise the Checker class and test some methods. 

Any compromised passwords will be reported:


* www.example.com
  * complexity
    * Password too short
    * There is no symbol
  * revision date
    * Password too old ( > 365 days)

**98 % of password are strong**

## License
This project is [GPL 3](https://www.gnu.org/licenses/gpl-3.0.html) licensed.
